-- select all from outlet_products table
SELECT id, created_at, updated_at, deleted_at, outlet_id, product_id, price FROM majoo.outlet_products;

-- select all from outlets table
SELECT id, created_at, updated_at, deleted_at, name, address, user_id FROM majoo.outlets;

-- select all from products table
SELECT id, created_at, updated_at, deleted_at, name, sku, display_image, user_id FROM majoo.products;

-- select all from products table where condition
SELECT id, created_at, updated_at, deleted_at, name, sku, display_image, user_id FROM majoo.products WHERE name like '%Product%';

-- select one from products table
SELECT id, created_at, updated_at, deleted_at, name, sku, display_image, user_id FROM majoo.products WHRE id=1;

-- insert records into products table
INSERT INTO majoo.products (created_at, updated_at, deleted_at, name, sku, display_image, user_id) 
VALUES(
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'Product 1', 'tcm-001', '/images/upload-102195360.png', 2,
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'Product 2', 'tcm-002', '/images/upload-105118872.png', 2,
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'Product 3', 'tcm-003', '/images/upload-211763003.png', 2
);

-- update record from products table
UPDATE majoo.products SET created_at='2021-06-26 16:54:57.880000000', updated_at='2021-06-26 16:54:57.880000000', name='Product 1', sku='tcm-001', display_image='/images/upload-102195360.png', user_id=2 WHERE id=1;

-- delete record from orducts table
UPDATE majoo.products SET deleted_at='2021-06-26 16:54:57.880000000' WHERE id=1;

-- select all from roles table
SELECT id, created_at, updated_at, deleted_at, description FROM majoo.roles;

-- select all from users table
SELECT id, created_at, updated_at, deleted_at, username, password, email, first_name, last_name, role_id FROM majoo.users;

-- select all from users table where condition
SELECT id, created_at, updated_at, deleted_at, username, password, email, first_name, last_name, role_id FROM majoo.users WHERE username='user_user';

-- select one from users table
SELECT id, created_at, updated_at, deleted_at, username, password, email, first_name, last_name, role_id FROM majoo.users WHERE id=1;

-- insert records into user table
INSERT INTO majoo.users (created_at, updated_at, deleted_at, username, password, email, first_name, last_name, role_id) 
VALUES(
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'user_admin', '$2y$10$S7nC9C3D1xv.gWqYo3BhWeVpO0DFxBYnrR0nt3ltYmXBp1ngHNtvO', 'user@admin.com', 'Admin', 'User', 1,
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'user_merchant', '$2y$10$XunZfZiPYes/zltuTrNZyexwGYsHSFbd0F/GB.Xh3idZChZE4myVK', 'user@merchant.com', 'Merchant', 'User', 2,
    '2021-06-26 16:54:57.880000000', '2021-06-26 16:54:57.880000000', null, 'user_user', '$2y$10$oCRGaResrjm9f2hD34F8Kusa27e0hq64G.Ht8GVoOfixzJesh76lW', 'user@user.com', 'User', 'User', 3
);

-- update record from users table
UPDATE majoo.users SET first_name='Admin', last_name='User' WHERE id=1;

-- delete record from users table
UPDATE majoo.users SET deleted_at='2021-06-26 16:54:57.880000000', WHERE id='1';
