package middlewares

import (
	"net/http"
	"testcasemajoo/app/auths"
	"testcasemajoo/app/helpers"
	"testcasemajoo/app/models"

	"github.com/gin-gonic/gin"
)

var CurrentUser models.User
var Meta auths.AccessDetails

func GetUser(c *gin.Context) {

	token, err := auths.ExtractTokenMetadata(c.Request)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"success": false,
			"message": "unauthorized",
		})
		return
	}
	Meta = *token
	userID, err := auths.FetchAuth(c, token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"success": false,
			"message": "unauthorized",
		})
		return
	}

	params := models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{userID},
	}

	CurrentUser, err = models.User{}.First(params)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"message": "missing user",
		})
		return
	}
}

func AuthMaster(c *gin.Context) {

	if CurrentUser.RoleID != helpers.MASTERROLE {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"success": false,
			"message": "unauthorized",
		})
		return
	}

}

func AuthMerchant(c *gin.Context) {

	role := CurrentUser.RoleID

	authorized := role == helpers.MASTERROLE || role == helpers.MERCHANTROLE

	if !authorized {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"success": false,
			"message": "unauthorized",
		})
		return
	}

}

func AuthUser(c *gin.Context) {

	role := CurrentUser.RoleID

	authorized := role == helpers.MASTERROLE || role == helpers.USERROLE

	if !authorized {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"success": false,
			"message": "unauthorized",
		})
		return
	}

}
