package configs

import (
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

// dbConf configuration.
var dbConf ConfDB

// ConfDB struct.
type ConfDB struct {
	Username string
	Password string
	Host     string
	Port     string
	Name     string
}

// SetDBConf method.
func SetDBConf(conf ConfDB) *gorm.DB {
	dbConf = conf

	return connection()
}

// connection mysql function to connecting to database
func connection() *gorm.DB {

	log.Println("initiate database connection")

	conn := dbConf.Username + ":" + dbConf.Password + "@tcp(" + dbConf.Host + ":" + dbConf.Port + ")/" + dbConf.Name + "?charset=utf8mb4&parseTime=True&loc=Local"

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,   // Slow SQL threshold
			LogLevel:      logger.Silent, // Log level
			Colorful:      false,         // Disable color
		},
	)

	db, errConn := gorm.Open(mysql.Open(conn), &gorm.Config{
		Logger: newLogger,
	})

	if errConn != nil {
		panic("failed to connect to database!")
	} else {
		log.Println("database connection success!")
	}

	Migration(db)
	Seeding(db)

	// Enable sql debugger
	db = db.Debug()

	return db
}
