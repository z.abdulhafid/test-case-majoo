package configs

import (
	"log"
	"testcasemajoo/app/models"

	"gorm.io/gorm"
)

// Migration method.
func Migration(db *gorm.DB) error {
	var err error

	log.Println("migrating the database")
	if err = db.AutoMigrate(
		&models.Role{},
		&models.User{},
		&models.Product{},
		&models.Outlet{},
		&models.OutletProduct{},
	); err != nil {
		return err
	}

	return nil
}
