package configs

import (
	"log"
	"testcasemajoo/app/models"

	"gorm.io/gorm"
)

func Seeding(db *gorm.DB) error {
	var err error

	seedRole(db)
	seedUser(db)
	seedProduct(db)
	seedOutlet(db)
	seedOutletProduct(db)

	return err
}

func seedRole(db *gorm.DB) {
	var roles []models.Role

	admin := models.Role{
		Description: "admin",
	}

	merchat := models.Role{
		Description: "merchant",
	}

	user := models.Role{
		Description: "user",
	}

	roles = append(roles, admin, merchat, user)

	for _, role := range roles {
		var isSaved models.Role
		var err error
		db.Where("description like ?", "%"+role.Description+"%").First(&isSaved).Debug()

		if isSaved == (models.Role{}) {
			err = db.Create(&role).Error
		}

		if err != nil {
			log.Println("can't create role:", role.Description)
		}
	}
}

func seedUser(db *gorm.DB) {
	var users []models.User

	admin := models.User{
		Username:  "user_admin",
		Password:  "$2y$10$S7nC9C3D1xv.gWqYo3BhWeVpO0DFxBYnrR0nt3ltYmXBp1ngHNtvO",
		Email:     "user@admin.com",
		FirstName: "Admin",
		LastName:  "User",
		RoleID:    1,
	}

	merchat := models.User{
		Username:  "user_merchant",
		Password:  "$2y$10$XunZfZiPYes/zltuTrNZyexwGYsHSFbd0F/GB.Xh3idZChZE4myVK",
		Email:     "user@merchant.com",
		FirstName: "Merchant",
		LastName:  "User",
		RoleID:    2,
	}

	user := models.User{
		Username:  "user_user",
		Password:  "$2y$10$oCRGaResrjm9f2hD34F8Kusa27e0hq64G.Ht8GVoOfixzJesh76lW",
		Email:     "user@user.com",
		FirstName: "User",
		LastName:  "User",
		RoleID:    3,
	}

	users = append(users, admin, merchat, user)

	for _, user := range users {
		var isSaved models.User
		var err error
		db.Where("username like ?", "%"+user.Username+"%").First(&isSaved).Debug()

		if err := user.Validate(); err != nil {
			log.Println("can't create user:", err.Error())
			continue
		}

		if isSaved == (models.User{}) {
			err = db.Create(&user).Error
		}

		if err != nil {
			log.Println("can't create user:", user.Username)
		}
	}
}

func seedProduct(db *gorm.DB) {
	var products []models.Product

	prod1 := models.Product{
		Name:         "Product 1",
		SKU:          "tcm-001",
		DisplayImage: "/images/upload-102195360.png",
		UserID:       2,
	}

	prod2 := models.Product{
		Name:         "Product 2",
		SKU:          "tcm-002",
		DisplayImage: "/images/upload-105118872.png",
		UserID:       2,
	}

	prod3 := models.Product{
		Name:         "Product 3",
		SKU:          "tcm-003",
		DisplayImage: "/images/upload-211763003.png",
		UserID:       2,
	}

	products = append(products, prod1, prod2, prod3)

	for _, product := range products {
		var isSaved models.Product
		var err error
		db.Where("sku like ?", product.SKU).First(&isSaved).Debug()

		if isSaved == (models.Product{}) {
			err = db.Create(&product).Error
		}

		if err != nil {
			log.Println("can't create product:", product.SKU)
		}
	}
}

func seedOutlet(db *gorm.DB) {
	var outlets []models.Outlet

	admin := models.Outlet{
		Name:    "Outlet 1",
		Address: "Address 1",
		UserID:  2,
	}

	merchat := models.Outlet{
		Name:    "Outlet 2",
		Address: "Address 2",
		UserID:  2,
	}

	user := models.Outlet{
		Name:    "Outlet 3",
		Address: "Address 3",
		UserID:  2,
	}

	outlets = append(outlets, admin, merchat, user)

	for _, outlet := range outlets {
		var isSaved models.Outlet
		var err error
		db.Where("sku like ?", outlet.Name).First(&isSaved).Debug()

		if isSaved == (models.Outlet{}) {
			err = db.Create(&outlet).Error
		}

		if err != nil {
			log.Println("can't create outlet:", outlet.Name)
		}
	}
}

func seedOutletProduct(db *gorm.DB) {
	outletProducts := []models.OutletProduct{
		{
			OutletID:  1,
			ProductID: 1,
			Price:     10000,
		},
		{
			OutletID:  1,
			ProductID: 2,
			Price:     11000,
		},
		{
			OutletID:  1,
			ProductID: 3,
			Price:     12000,
		},
		{
			OutletID:  2,
			ProductID: 1,
			Price:     12000,
		},
		{
			OutletID:  2,
			ProductID: 2,
			Price:     11000,
		},
		{
			OutletID:  2,
			ProductID: 3,
			Price:     10000,
		},
		{
			OutletID:  3,
			ProductID: 1,
			Price:     11000,
		},
		{
			OutletID:  3,
			ProductID: 2,
			Price:     12000,
		},
		{
			OutletID:  3,
			ProductID: 3,
			Price:     10000,
		},
	}

	for _, outletProduct := range outletProducts {
		var isSaved models.OutletProduct
		var err error
		db.Where("outlet_id = ? AND product_id = ?", outletProduct.OutletID, outletProduct.ProductID).First(&isSaved).Debug()

		if isSaved == (models.OutletProduct{}) {
			err = db.Create(&outletProduct).Error
		}

		if err != nil {
			log.Println("can't create outlet product:", outletProduct.OutletID, outletProduct.ProductID)
		}
	}
}
