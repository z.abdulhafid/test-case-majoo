package models

import (
	"github.com/go-playground/validator/v10"
	"gorm.io/gorm"
)

var DB *gorm.DB
var V *validator.Validate

type FindParams struct {
	Page        int
	Limit       int
	SearchQuery string
	In          []struct {
		Column string
		Values []interface{}
	}
	Order []struct {
		Column string
		Desc   bool
	}
}

type FirstParams struct {
	Condition string
	Column    []string
	Value     []interface{}
}
