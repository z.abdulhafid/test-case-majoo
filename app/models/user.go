package models

import (
	"fmt"
	"strings"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// User model.
type User struct {
	gorm.Model
	Username  string `gorm:"type:varchar(255);not null;unique" json:"username" validate:"min=3,max=40"`
	Password  string `gorm:"type:varchar(100);not null;" json:"password" validate:"passwd"`
	Email     string `gorm:"type:varchar(100);not null;unique" json:"email" validate:"required,email"`
	FirstName string `gorm:"type:varchar(255);" json:"first_name"`
	LastName  string `gorm:"type:varchar(255);" json:"last_name"`
	RoleID    uint   `gorm:"type:bigint(20) unsigned;index:idx_users_role_id;not null" json:"role_id" validate:"required"`
}

func (user User) Find(params FindParams) ([]User, error) {

	var (
		users  []User
		err    error
		total  int64
		page   = params.Page
		limit  = params.Limit
		ins    = params.In
		orders = params.Order
	)

	query := DB.Model(&user)

	if search := strings.Split(params.SearchQuery, "="); len(search) == 2 {
		query = query.Where(search[0], search[1])
	}

	for _, str := range ins {
		column := str.Column
		value := str.Values
		query = query.Where(fmt.Sprintf("%s IN ?", column), value...)
	}

	for _, order := range orders {
		name := order.Column
		desc := order.Desc

		query = query.Order(clause.OrderByColumn{Column: clause.Column{Name: name}, Desc: desc})
	}

	query.Count(&total)

	query = query.Limit(limit)

	offset := (page - 1) * limit
	if offset > 0 {
		query = query.Offset(offset)
	}

	if err := query.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, err
}

func (user User) First(params FirstParams) (User, error) {

	var (
		condition = params.Condition
		value     = params.Value
	)

	column := make([]interface{}, len(params.Column))
	for i, v := range params.Column {
		column[i] = v
	}

	err := DB.Where(fmt.Sprintf(condition, column...), value...).First(&user).Error

	return user, err
}

func (user User) Create() (User, error) {

	if err := user.Validate(); err != nil {
		return User{}, err
	}

	hashed, err := hash(user.Password)
	if err != nil {
		return User{}, err
	}

	user.Password = string(hashed)

	err = DB.Create(&user).Error

	return user, err
}

func (user User) Update(newUser User) (User, error) {

	err := DB.Model(&user).Updates(newUser).Error

	return user, err
}

func (user User) ChangePassword(newPassword string) (User, error) {

	var (
		newUser = user
		err     error
	)

	newUser.Password = newPassword
	if err := newUser.Validate(); err != nil {
		return user, err
	}

	hashed, err := hash(newPassword)
	if err != nil {
		return user, err
	}

	newUser.Password = string(hashed)

	err = DB.Model(&user).Updates(newUser).Error

	return user, err

}

func (user User) Delete() error {
	return DB.Delete(&user).Error
}

func (user User) Validate() error {
	return V.Struct(user)
}

func (user User) VerifyPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
}

func hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
