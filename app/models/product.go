package models

import (
	"fmt"
	"strings"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

// Product Model.
type Product struct {
	gorm.Model
	Name         string `gorm:"type:varchar(255);not null;" json:"name" validate:"required"`
	SKU          string `gorm:"type:varchar(255);not null;" json:"sku" validate:"required"`
	DisplayImage string `gorm:"type:varchar(255);not null;" json:"display_image" validate:"required,file"`
	UserID       uint   `gorm:"type:bigint(20) unsigned;index:idx_products_user_id;not null;" json:"user_id" validate:"required"`
}

func (product Product) Find(params FindParams) ([]Product, error) {

	var (
		products []Product
		err      error
		total    int64
		page     = params.Page
		limit    = params.Limit
		ins      = params.In
		orders   = params.Order
	)

	query := DB.Model(&product)

	if search := strings.Split(params.SearchQuery, "="); len(search) == 2 {
		query = query.Where(search[0], search[1])
	}

	for _, str := range ins {
		column := str.Column
		value := str.Values
		query = query.Where(fmt.Sprintf("%s IN ?", column), value...)
	}

	for _, order := range orders {
		name := order.Column
		desc := order.Desc

		query = query.Order(clause.OrderByColumn{Column: clause.Column{Name: name}, Desc: desc})
	}

	query.Count(&total)

	query = query.Limit(limit)

	offset := (page - 1) * limit
	if offset > 0 {
		query = query.Offset(offset)
	}

	if err := query.Find(&products).Error; err != nil {
		return nil, err
	}

	return products, err
}

func (product Product) First(params FirstParams) (Product, error) {

	var (
		condition = params.Condition
		value     = params.Value
	)

	column := make([]interface{}, len(params.Column))
	for i, v := range params.Column {
		column[i] = v
	}

	err := DB.Where(fmt.Sprintf(condition, column...), value...).First(&product).Error

	return product, err
}

func (product Product) Create() (Product, error) {

	err := product.Validate()
	if err != nil {
		return product, err
	}

	err = DB.Create(&product).Error

	return product, err
}

func (product Product) Update(newProduct Product) (Product, error) {

	err := newProduct.Validate()
	if err != nil {
		return product, err
	}

	err = DB.Model(&product).Updates(newProduct).Error

	return product, err
}

func (product Product) Delete() error {

	err := DB.Delete(&product).Error

	return err
}

func (product Product) Validate() error {
	return V.Struct(product)
}
