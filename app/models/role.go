package models

import "gorm.io/gorm"

// Role Model.
type Role struct {
	gorm.Model
	Description string `gorm:"type:varchar(100)" json:"description"`
}

func (role Role) Validate() error {
	return V.Struct(role)
}
