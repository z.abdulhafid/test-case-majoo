package models

import "gorm.io/gorm"

type Outlet struct {
	gorm.Model
	Name    string `gorm:"type:varchar(255);not null" json:"name" validate:"required"`
	Address string `gorm:"type:longtext" json:"address"`
	UserID  uint   `gorm:"type:bigint(20) unsigned;index:idx_outlets_user_id;not null;" json:"user_id"`
}

type OutletProduct struct {
	gorm.Model
	OutletID  uint  `gorm:"type:bigint(20) unsigned;index:idx_outlet_products_outlet_id;not null;" json:"outlet_id"`
	ProductID uint  `gorm:"type:bigint(20) unsigned;index:idx_outlet_products_product_id;not null;" json:"product_id"`
	Price     int32 `gorm:"type:int(11)" json:"price"`
}

func (outlet Outlet) Validate() error {
	return V.Struct(outlet)
}
