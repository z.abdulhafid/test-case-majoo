package routes

import (
	"testcasemajoo/app/controllers"
	mw "testcasemajoo/app/middlewares"

	"github.com/gin-gonic/gin"
)

// AppRoute methode.
func AppRoute(r *gin.Engine) {

	uri := r.Group("/api/v1")

	ctrl := controllers.Controller{}

	{
		uri.POST("/login", ctrl.Login)
		uri.POST("/register", ctrl.Register)
		uri.POST("/refresh", ctrl.Refresh)
		uri.POST("/logout", mw.GetUser, controllers.Controller{
			CurrentUser: &mw.CurrentUser,
			Meta:        &mw.Meta,
		}.Logout)
	}

	uri.Use(mw.GetUser)
	ctrl.CurrentUser = &mw.CurrentUser
	ctrl.Meta = &mw.Meta

	user := uri.Group("/user")
	{
		userCtrl := ctrl
		user.GET("/", mw.AuthMaster, userCtrl.ListUser)
		user.GET("/:id", userCtrl.ShowUser)
		user.PUT("/:id", userCtrl.UpdateUser)
		user.DELETE("/:id", mw.AuthMaster, userCtrl.DeleteUser)
	}

	product := uri.Group("/product")
	product.Use(mw.AuthMerchant)
	{
		productCtrl := ctrl
		product.GET("/", productCtrl.ListProduct)
		product.POST("", productCtrl.CreateProduct)
		product.GET("/:id", productCtrl.ShowProduct)
		product.PUT("/:id", productCtrl.UpdateProduct)
		product.DELETE("/:id", productCtrl.DeleteProduct)
	}

}
