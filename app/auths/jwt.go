package auths

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
	"github.com/twinj/uuid"
)

var client *redis.Client

type TokenDetails struct {
	AccessToken      string `json:"access_token"`
	RefreshToken     string `json:"refresh_token"`
	AccessUUID       string `json:"access_uuid"`
	RefreshUUID      string `json:"refresh_uuid"`
	AccessExpiredAt  int64  `json:"access_expired_at"`
	RefreshExpiredAt int64  `json:"refresh_expired_at"`
}

type AccessDetails struct {
	AccessUUID string
	UserID     uint64
}

func Redis(c *gin.Context) {
	//Initializing redis
	dsn := os.Getenv("REDIS_DSN")
	if len(dsn) == 0 {
		dsn = "localhost:6379"
	}
	client = redis.NewClient(&redis.Options{
		Addr: dsn, //redis port
	})

	_, err := client.Ping(c).Result()
	if err != nil {
		panic(err)
	}
}

func CreateToken(userID uint) (*TokenDetails, error) {
	var err error

	td := &TokenDetails{}
	td.AccessExpiredAt = time.Now().Add(time.Minute * 15).Unix()
	td.AccessUUID = uuid.NewV4().String()

	td.RefreshExpiredAt = time.Now().Add(time.Hour * 24 * 7).Unix()
	td.RefreshUUID = uuid.NewV4().String()

	//Creating Access Token
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["access_uuid"] = td.AccessUUID
	atClaims["user_id"] = userID
	atClaims["exp"] = td.AccessExpiredAt
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	td.AccessToken, err = at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return nil, err
	}
	//Creating Refresh Token
	rtClaims := jwt.MapClaims{}
	rtClaims["refresh_uuid"] = td.RefreshUUID
	rtClaims["user_id"] = userID
	rtClaims["exp"] = td.RefreshExpiredAt
	rt := jwt.NewWithClaims(jwt.SigningMethodHS256, rtClaims)
	td.RefreshToken, err = rt.SignedString([]byte(os.Getenv("REFRESH_SECRET")))
	if err != nil {
		return nil, err
	}
	return td, nil
}

func CreateAuth(c *gin.Context, userID uint, td *TokenDetails) error {
	at := time.Unix(td.AccessExpiredAt, 0) //converting Unix to UTC(to Time object)
	rt := time.Unix(td.RefreshExpiredAt, 0)
	now := time.Now()

	errAccess := client.Set(c, td.AccessUUID, strconv.Itoa(int(userID)), at.Sub(now)).Err()
	if errAccess != nil {
		return errAccess
	}
	errRefresh := client.Set(c, td.RefreshUUID, strconv.Itoa(int(userID)), rt.Sub(now)).Err()
	if errRefresh != nil {
		return errRefresh
	}
	return nil
}

func ExtractToken(r *http.Request) string {
	bearToken := r.Header.Get("Authorization")
	//normally Authorization the_token_xxx
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func VerifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func TokenValid(r *http.Request) error {
	token, err := VerifyToken(r)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}

func ExtractTokenMetadata(r *http.Request) (*AccessDetails, error) {
	token, err := VerifyToken(r)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		accessUUID, ok := claims["access_uuid"].(string)
		if !ok {
			return nil, err
		}
		userID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
		if err != nil {
			return nil, err
		}
		return &AccessDetails{
			AccessUUID: accessUUID,
			UserID:     userID,
		}, nil
	}
	return nil, err
}

func FetchAuth(c *gin.Context, authD *AccessDetails) (uint, error) {
	userid, err := client.Get(c, authD.AccessUUID).Result()
	if err != nil {
		return 0, err
	}
	userID, _ := strconv.ParseUint(userid, 10, 64)
	return uint(userID), nil
}

func DeleteAuth(c *gin.Context, givenUUID string) (int64, error) {
	log.Println(givenUUID)

	deleted, err := client.Del(c, givenUUID).Result()
	if err != nil {
		return 0, err
	}
	return deleted, nil
}

func Refresh(c *gin.Context, refreshToken string) (*TokenDetails, error) {

	var (
		td *TokenDetails
	)

	token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("REFRESH_SECRET")), nil
	})
	if err != nil {
		// return td, errors.New("refresh token expired")
		return td, err
	}

	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return td, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		refreshUUID, ok := claims["refresh_uuid"].(string)
		if !ok {
			return td, err
		}
		userID, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["user_id"]), 10, 64)
		if err != nil {
			return td, errors.New("error occurred")
		}
		//Delete the previous Refresh Token
		deleted, delErr := DeleteAuth(c, refreshUUID)
		if delErr != nil || deleted == 0 { //if any goes wrong
			return td, errors.New("unauthorized")
		}
		//Create new pairs of refresh and access tokens
		td, createErr := CreateToken(uint(userID))
		if createErr != nil {
			return td, createErr
		}
		//save the tokens metadata to redis
		saveErr := CreateAuth(c, uint(userID), td)
		if saveErr != nil {
			return td, saveErr
		}

		return td, nil
	}
	return td, errors.New("refresh expired")
}
