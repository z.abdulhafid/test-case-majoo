package controllers

import (
	"net/http"
	"strconv"
	"testcasemajoo/app/helpers"
	"testcasemajoo/app/models"

	"github.com/gin-gonic/gin"
)

func (ctrl Controller) ListProduct(c *gin.Context) {

	var (
		searchQuery string
		products    []models.Product
		err         error
	)

	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "10"))

	name := c.DefaultQuery("name", "")
	if name != "" {
		searchQuery = "name=" + name
	}

	products, err = models.Product{}.Find(models.FindParams{
		Page:        page,
		Limit:       limit,
		SearchQuery: searchQuery,
	})
	if err != nil {
		InternalServerError(c, "finding error")
	}

	Success(c, products)

}

func (ctrl Controller) ShowProduct(c *gin.Context) {

	var (
		id      string
		product models.Product
		err     error
	)

	id = c.Param("id")
	product, err = models.Product{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
		return
	}

	Success(c, product)

}

func (ctrl Controller) CreateProduct(c *gin.Context) {

	var (
		product models.Product
		err     error
		r       = c.Request
	)

	product.Name = c.PostForm("name")
	product.SKU = c.PostForm("sku")
	product.UserID = ctrl.CurrentUser.ID

	file, err := uploadImage(r, "display_image")
	if err != nil {
		BadRequest(c, err.Error())
		return
	}

	product.DisplayImage = file
	product, err = product.Create()
	if err != nil {
		InternalServerError(c, err.Error())
		return
	}

	Success(c, product)

}

func (ctrl Controller) UpdateProduct(c *gin.Context) {

	var (
		id      string
		product models.Product
		updated models.Product
		r       = c.Request
		err     error
	)

	id = c.Param("id")
	product, err = models.Product{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
		return
	}

	if ctrl.CurrentUser.RoleID != helpers.MASTERROLE && ctrl.CurrentUser.ID != product.UserID {
		Unauthorized(c, "unauthorized")
		return
	}

	updated = product

	if c.PostForm("name") != "" {
		updated.Name = c.PostForm("name")
	}
	if c.PostForm("sku") != "" {
		updated.SKU = c.PostForm("sku")
	}

	file, err := uploadImage(r, "display_image")
	if err != nil {
		BadRequest(c, err.Error())
		return
	}

	if file != "" {
		updated.DisplayImage = file
	}

	product, err = product.Update(updated)
	if err != nil {
		InternalServerError(c, "update failed")
		return
	}

	Success(c, product)

}

func (ctrl Controller) DeleteProduct(c *gin.Context) {

	var (
		id      string
		product models.Product
		err     error
	)

	id = c.Param("id")
	product, err = models.Product{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
		return
	}

	err = product.Delete()
	if err != nil {
		InternalServerError(c, err.Error())
		return
	}

	c.JSON(http.StatusOK, "deleted")
}
