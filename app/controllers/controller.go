package controllers

import (
	"errors"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"testcasemajoo/app/auths"
	"testcasemajoo/app/models"

	"github.com/gin-gonic/gin"
)

// Controller struct.
type Controller struct {
	CurrentUser *models.User
	Meta        *auths.AccessDetails
}

func BadRequest(c *gin.Context, err string) {
	c.JSON(http.StatusBadRequest, gin.H{
		"success": false,
		"message": err,
	})
}

func Unauthorized(c *gin.Context, err string) {
	c.JSON(http.StatusUnauthorized, gin.H{
		"success": false,
		"message": err,
	})
}

func Forbidden(c *gin.Context, err string) {
	c.JSON(http.StatusForbidden, gin.H{
		"success": false,
		"message": err,
	})
}

func InternalServerError(c *gin.Context, err string) {
	c.JSON(http.StatusInternalServerError, gin.H{
		"success": false,
		"message": err,
	})
}

func NotFound(c *gin.Context, err string) {
	c.JSON(http.StatusNotFound, gin.H{
		"success": false,
		"message": err,
	})
}

func Success(c *gin.Context, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"data":    data,
	})
}

func Found(c *gin.Context, data interface{}) {
	c.JSON(http.StatusFound, gin.H{
		"success": true,
		"data":    data,
	})
}

func Created(c *gin.Context, data interface{}) {
	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"data":    data,
	})
}

func validateFile(handler *multipart.FileHeader) error {
	var (
		fileName = handler.Filename
		size     = handler.Size
		mime     = handler.Header
		MAX_SIZE = 20 * 1024 * 1024
	)

	log.Println(fileName)
	if size > int64(MAX_SIZE) {
		return errors.New("file too big")
	}

	rawExt := strings.Split(mime["Content-Type"][0], "/")
	switch rawExt[1] {
	case "png", "jpg", "jpeg":
		return nil
	default:
		return errors.New("wrong file type")
	}

}

func uploadImage(r *http.Request, form string) (string, error) {

	var (
		file      multipart.File
		handler   *multipart.FileHeader
		tempFile  *os.File
		fileBytes []byte
		err       error
	)

	file, handler, err = r.FormFile(form)

	switch err {
	case nil:
		// do nothing
	case http.ErrMissingFile:
		return "", nil
	default:
		return "", err
	}

	defer file.Close()
	if err = validateFile(handler); err != nil {
		return "", err
	}

	wd, _ := os.Getwd()
	tempFile, err = ioutil.TempFile(wd+"/images", "upload-*.png")
	if err != nil {
		return "", err
	}
	defer tempFile.Close()

	fileBytes, err = ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}
	_, err = tempFile.Write(fileBytes)

	return tempFile.Name(), nil
}
