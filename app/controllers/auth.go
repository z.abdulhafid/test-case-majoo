package controllers

import (
	"net/http"
	"testcasemajoo/app/auths"
	"testcasemajoo/app/models"

	"github.com/gin-gonic/gin"
)

type loginRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

func (ctrl Controller) Login(c *gin.Context) {

	var (
		request loginRequest
		err     error
	)

	if err = c.Bind(&request); err != nil {
		BadRequest(c, "invalid json provided")
		return
	}

	params := models.FirstParams{
		Condition: "%s LIKE ?",
		Column:    []string{"username"},
		Value:     []interface{}{request.Username},
	}

	user, err := models.User{}.First(params)
	if err != nil {
		Unauthorized(c, "invalid login details")
		return
	}

	if err := user.VerifyPassword(request.Password); err != nil {
		Unauthorized(c, "invalid login details")
		return
	}

	td, err := auths.CreateToken(user.ID)
	if err != nil {
		BadRequest(c, err.Error())
		return
	}

	saveErr := auths.CreateAuth(c, user.ID, td)
	if saveErr != nil {
		BadRequest(c, saveErr.Error())
		return
	}

	Success(c, td)

}

func (ctrl Controller) Logout(c *gin.Context) {
	deleted, delErr := auths.DeleteAuth(c, ctrl.Meta.AccessUUID)
	if delErr != nil || deleted == 0 { //if any goes wrong
		Unauthorized(c, "unauthorized")
		return
	}
	c.JSON(http.StatusOK, "logged out")
}

func (ctrl Controller) Refresh(c *gin.Context) {

	var (
		mapToken map[string]string
		err      error
	)

	if err = c.ShouldBindJSON(&mapToken); err != nil {
		c.JSON(http.StatusUnprocessableEntity, err.Error())
		return
	}
	refreshToken := mapToken["refresh_token"]

	td, err := auths.Refresh(c, refreshToken)
	if err != nil {
		Forbidden(c, err.Error())
		return
	}

	Success(c, td)
}
