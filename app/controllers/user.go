package controllers

import (
	"net/http"
	"strconv"
	"testcasemajoo/app/helpers"
	"testcasemajoo/app/models"

	"github.com/gin-gonic/gin"
)

type UserRequest struct {
	FirstName string `json:"first_name" binding:"required"`
	LastName  string `json:"last_name"`
}

type UserRegister struct {
	Username  string `json:"username" binding:"required"`
	Password  string `json:"password" binding:"required"`
	Email     string `json:"email" binding:"required"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

func (ctrl Controller) ListUser(c *gin.Context) {

	var (
		searchQuery string
		users       []models.User
		err         error
	)

	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "10"))

	username := c.DefaultQuery("username", "")
	if username != "" {
		searchQuery = "username=" + username
	}

	users, err = models.User{}.Find(models.FindParams{
		Page:        page,
		Limit:       limit,
		SearchQuery: searchQuery,
	})
	if err != nil {
		InternalServerError(c, "finding error")
	}

	Success(c, users)

}

func (ctrl Controller) ShowUser(c *gin.Context) {

	var (
		id   string
		user models.User
		err  error
	)

	id = c.Param("id")
	user, err = models.User{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
		return
	}

	if ctrl.CurrentUser.RoleID != helpers.MASTERROLE && ctrl.CurrentUser.ID != user.ID {
		Unauthorized(c, "unauthorized")
		return
	}

	Success(c, user)

}

func (ctrl Controller) Register(c *gin.Context) {

	var (
		user    models.User
		request UserRegister
		err     error
	)

	if err = c.Bind(&request); err != nil {
		BadRequest(c, "invalid json provided")
		return
	}

	user = models.User{
		Username:  request.Username,
		Password:  request.Password,
		Email:     request.Email,
		FirstName: request.FirstName,
		LastName:  request.LastName,
		RoleID:    helpers.USERROLE,
	}
	user, err = user.Create()
	if err != nil {
		InternalServerError(c, err.Error())
		return
	}

	Success(c, user)

}

func (ctrl Controller) UpdateUser(c *gin.Context) {

	var (
		id      string
		user    models.User
		updated models.User
		request UserRequest
		err     error
	)

	id = c.Param("id")
	user, err = models.User{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
		return
	}

	if ctrl.CurrentUser.RoleID != helpers.MASTERROLE && ctrl.CurrentUser.ID != user.ID {
		Unauthorized(c, "unauthorized")
		return
	}

	if err = c.Bind(&request); err != nil {
		BadRequest(c, "invalid json provided")
		return
	}

	updated = models.User{
		FirstName: request.FirstName,
		LastName:  request.LastName,
	}
	user, err = user.Update(updated)
	if err != nil {
		InternalServerError(c, "update failed")
		return
	}

	Success(c, user)

}

func (ctrl Controller) DeleteUser(c *gin.Context) {

	var (
		id   string
		user models.User
		err  error
	)

	id = c.Param("id")
	user, err = models.User{}.First(models.FirstParams{
		Condition: "%s = ?",
		Column:    []string{"id"},
		Value:     []interface{}{id},
	})
	if err != nil {
		NotFound(c, err.Error())
	}

	err = user.Delete()
	if err != nil {
		InternalServerError(c, err.Error())
	}

	c.JSON(http.StatusOK, "deleted")
}
