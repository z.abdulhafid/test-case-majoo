package validators

import "github.com/go-playground/validator/v10"

func Validator() *validator.Validate {
	v := validator.New()

	v.RegisterValidation("passwd", func(fl validator.FieldLevel) bool {
		return len(fl.Field().String()) > 8
	})

	return v
}
