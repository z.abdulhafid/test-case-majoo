module testcasemajoo

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/gin-gonic/gin v1.7.2 // indirect
	github.com/go-playground/validator/v10 v10.6.1 // indirect
	github.com/go-redis/redis/v8 v8.10.0 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/twinj/uuid v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.11
)
