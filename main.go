package main

import (
	"log"
	"os"
	"strconv"

	"testcasemajoo/app/auths"
	"testcasemajoo/app/configs"
	"testcasemajoo/app/helpers"
	"testcasemajoo/app/models"
	"testcasemajoo/app/routes"
	v "testcasemajoo/app/validators"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gorm.io/gorm"
)

func main() {
	envPath := ".env"
	if err := godotenv.Load(envPath); err != nil {
		log.Fatalf("Error loading .env file")
	}

	// Make models validations
	models.V = v.Validator()

	// Make database connection
	models.DB = configs.SetDBConf(configs.ConfDB{
		Username: os.Getenv("USER_DATABASE"),
		Password: os.Getenv("PASS_DATABASE"),
		Host:     os.Getenv("HOST_DATABASE"),
		Port:     os.Getenv("PORT_DATABASE"),
		Name:     os.Getenv("NAME_DATABASE"),
	})

	USERROLE, _ := strconv.Atoi(os.Getenv("USER_ROLE"))
	MERCHANTROLE, _ := strconv.Atoi(os.Getenv("MERCHANT_ROLE"))
	MASTERROLE, _ := strconv.Atoi(os.Getenv("MASTER_ROLE"))

	helpers.USERROLE = uint(USERROLE)
	helpers.MERCHANTROLE = uint(MERCHANTROLE)
	helpers.MASTERROLE = uint(MASTERROLE)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	// Running the service
	run(models.DB, port)

}

func run(db *gorm.DB, port string) {
	r := gin.Default()
	r.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{
			"success": false,
			"errors": []gin.H{
				{
					"type":    "validation",
					"source":  c.Request.URL.Path,
					"message": "Jalan tidak ditemukan",
				},
			},
		})
	})

	log.Println("Running on PORT:" + port)

	r.Use(auths.Redis)
	routes.AppRoute(r)

	r.Run(":" + port)

}
